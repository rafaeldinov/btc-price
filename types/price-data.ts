import { Currency } from './currency';

export type PriceData = {
  time:
    {
      updated: string;
      updatedISO: string;
      updateduk: string;
    },
    disclaimer: string;
    chartName: string;
    bpi: {
      USD: Currency;
      GBP: Currency;
      EUR:Currency;
  }
}
