import { Action, AnyAction, combineReducers, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { btcSlice } from './btc-reducer/btc-reducer';
import { createWrapper } from 'next-redux-wrapper';


export const makeStore = () =>
  configureStore({
    reducer: {
      btc: btcSlice.reducer
    }
  }
);

type Store = ReturnType<typeof makeStore>;

export type AppDispatch = Store['dispatch'];
export type RootState = ReturnType<Store['getState']>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;

export const wrapper = createWrapper(makeStore, {debug: true});

