import { State } from '../../types/state';
import { PriceData } from '../../types/price-data';

export const getBtcPrice = (state: State): PriceData | undefined => state['btc'].btcData;
