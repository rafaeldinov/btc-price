import { createSlice } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';
import { PriceData } from '../../types/price-data';

type InitialState = {
  btcData: PriceData | null;
}

const initialState: InitialState = {
  btcData: null,
};

export const btcSlice = createSlice({
  name: 'btc',
  initialState,
  reducers: {
    setPriceData: (state, action) => {
      state.btcData = action.payload;
    }
  },

  extraReducers: {
    [HYDRATE]: (state, action) => {
      return {
        ...state,
        ...action.payload.btc,
      };
    },
  },
});

export const { setPriceData } = btcSlice.actions;
